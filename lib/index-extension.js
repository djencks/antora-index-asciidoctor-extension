'use strict'

const picomatch = require('picomatch')
const camelcaseKeys = require('camelcase-keys')
const reportSupport = require('@djencks/asciidoctor-report-support')

const UNESCAPE_STAR = /\\\*/g

const INDEX_INCLUDE = 'index$'
const INDEX_COUNT_INCLUDE = 'indexcount$'
const INDEX_UNIQUE_COUNT_INCLUDE = 'indexuniquecount$'

let pipelineConfig = { playbookDir: undefined, debug: false, trace: false }

module.exports.register = function (registry, config) {
  const pc = reportSupport.pipelineConfigure(registry, config, module.exports, doQuery({}), '@djencks/asciidoctor-antora-indexer', enhanceTargetItem)
  if (pc) {
    //called as Antora extension register.
    pipelineConfig = pc
    return
  }

  //called as Asciidoctor extension register.

  function fixAttributes (attributes) {
    for (const key of ['version', 'component', 'module', 'family', 'relative']) {
      attributes[key] && (attributes[key] = attributes[key].replace(UNESCAPE_STAR, '*'))
    }
    return attributes
  }

  function doQuery (cache) {
    return function (target, attributes, fn, contentCatalog, file, logContext,
      contentExtractor = null, transform = (items) => items.sort(compare)) {
      const { filter, relCompare, attributesPresent, attributesMissing, attributeValues } =
        getFilter(fixAttributes(attributes), file.src, logContext)
      logContext && logContext.debug && logContext.logger.debug(Object.assign(
        { filter, attributesPresent, attributesMissing, attributeValues },
        attributes.relative ? { relative: attributes.relative, exclude: attributes.exclude } : {},
        logContext.use))
      let items = contentCatalog.findBy(filter)
        .filter((test) => (filter.family ||
            (test.src.family === 'page' && test !== file) ||
            (test.src.family === 'alias' && test.rel !== file)) &&
          test.src.component && //never include site start page alias
          relCompare(test.src.relative))
        .map((item) => attributesFromFile(item, contentExtractor, cache, logContext))
        .filter((item) =>
          attributesPresent.every((attributeName) => attributeName in item) &&
          attributesMissing.every((attributeName) => !(attributeName in item)) &&
          attributeValues.every(([name, value]) => item[name] === value)
        )

      transform = transform.bind({ contentCatalog, file })
      items = transform(items)

      items.forEach(fn)
    }
  }

  function getFilter (attrs, src, logContext) {
    const filter = reportSupport.baseFilter(src, attrs)
    const relCompare = attrs.relative ? picomatcher(attrs.relative, attrs.exclude) : () => true
    const attributesPresent = []
    const attributesMissing = []
    const attributeValues = []
    attrs.attributes && attrs.attributes.split(',').forEach((clause) => {
      const match = clause.split('=')
      if (match.length === 2) {
        attributeValues.push([match[0], match[1]])
      } else if (match.length === 1) {
        if (match[0].startsWith('!')) {
          attributesMissing.push(match[0].slice(1))
        } else {
          attributesPresent.push(match[0])
        }
      } else {
        logContext.logger.warn({ msg: `Invalid clause ${clause}`, ...logContext.use })
      }
    })
    return { filter, relCompare, attributesPresent, attributesMissing, attributeValues }
  }

  function picomatcher (relative, exclude) {
    const options = exclude ? { ignore: exclude.split(',').map((term) => term.trim()) } : {}
    return picomatch(relative.split(',').map((term) => term.trim()), options)
  }

  function formatFunction (cellAttr, userRequires, logContext) {
    return reportSupport.formatFunction(cellAttr, userRequires, logContext,
      (item) => Object.assign({ this: item }, camelcaseKeys(item)))
  }

  function enhanceTargetItem (match, logContext) {
    if (!match) {
      match = '(?<body>).*'
    }
    const matcher = picomatch(match)
    return (item) => {
      const relative = item.src.relative
      const m = matcher(relative, true)
      if (m.isMatch) {
        return { item: m.match.groups, path: item.src.path }
      }
      logContext.logger.warn(`no match for ${relative}`, logContext.use)
      return { item: { relative }, path: item.src.path }
    }
  }

  function doRegister (registry) {
    const reports = reportSupport('@djencks/asciidoctor-antora-indexer', doQuery({}), pipelineConfig, registry, config)
    if (typeof registry.block === 'function') {
      registry.block(reports.templateBlockProcessor('indexBlock', formatFunction, reports.processBlocks, false))
      registry.block(reports.templateBlockProcessor('indexTemplate', formatFunction, reports.processTemplates, false))
    } else {
      reports.logger.warn('No \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(reports.tableProcessor('indexTable', '|', formatFunction, false, true))
      registry.blockMacro(reports.descriptionListProcessor('indexDescriptionList', formatFunction, '$xref', false, true))
      registry.blockMacro(reports.listProcessor('indexList', 'ulist', '*', formatFunction, '$xref', false, true))
      registry.blockMacro(reports.listProcessor('indexOrderedList', 'olist', '.', formatFunction, '$xref', false, true))
      registry.blockMacro(reports.expressionProcessor('indexExpression', formatFunction, reports.evaluateExpressionBlock))
    } else {
      reports.logger.warn('No \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(reports.inlineCountProcessor('indexCount', false, true))
      registry.inlineMacro(reports.inlineUniqueCountProcessor('indexUniqueCount', formatFunction, false, true))
      registry.inlineMacro(reports.expressionProcessor('indexExpression', formatFunction, reports.evaluateExpressionInline))
    } else {
      reports.logger.warn('No \'inlineMacro\' method on alleged registry')
    }
    if (typeof registry.includeProcessor === 'function') {
      registry.prefer('include_processor', reports.attributesIncludeProcessor(INDEX_INCLUDE, formatFunction, false))
      registry.prefer('include_processor', reports.countAttributesIncludeProcessor(INDEX_COUNT_INCLUDE))
      registry.prefer('include_processor', reports.uniqueCountAttributesIncludeProcessor(INDEX_UNIQUE_COUNT_INCLUDE, formatFunction, false))
    } else {
      reports.logger.warn('No \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}

const compare = (f1, f2) => {
  const t1 = f1.doctitle.toLowerCase()
  const t2 = f2.doctitle.toLowerCase()
  return t1 < t2 ? -1 : t1 > t2 ? 1 : 0
}

function escapeRelative (relative) {
  // const orig = relative
  relative = relative.replace(/(__[^_]+(_[^_]+?)*__)/g, '\\$1')
  relative = relative.replace(/(\*\*[^*]+(\*[^*]+?)*\*\*)/g, '\\$1')
  // console.log(`${orig} >> ${relative}`)
  return relative
}

function xref (src) {
  const { component, version, module, relative } = src
  return `xref:${version || '_'}@${component}:${module}:${escapeRelative(relative)}[]`
}

function resourceId (src) {
  const { component, version, module, family, relative } = src
  return `${version || '_'}@${component}:${module}:${family}$${relative}`
}

function attributesFromFile (file, contentExtractor, cache, logContext) {
  file = file.src.family === 'alias' ? file.rel : file
  const src = file.src
  const $resourceid = resourceId(src)
  return Object.assign(
    { doctitle: src.relative, $xref: xref(src), $resourceid, src },
    contentExtractor ? { $: cache[$resourceid] || (cache[$resourceid] = contentExtractor(file, logContext)) } : {},
    file.asciidoc ? file.asciidoc.attributes : {}
  )
}
