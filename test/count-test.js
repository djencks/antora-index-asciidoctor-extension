'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension count tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkDoc (doc, count) {
    expect(doc).to.contain(`<p>${count}</p>`)
  }

  ;[
    {
      page: (filter) => `indexCount:[${filter}]`,
    },
    {
      page: (filter) => `indexUniqueCount:[format=description,${filter}]`,
    },
    {
      page: (filter) => `indexUniqueCount:[format=mod2,${filter}]`,
    },
    {
      page: (filter) => `indexUniqueCount:[format=mod3,${filter}]`,
    },
  ].forEach(({ page }, i) => {
    describe(`macro ${page('')} number ${i}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [10, 5, 2, 3][i])
      })

      it('component * filter test', () => {
        file.contents = Buffer.from(page('component=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [20, 5, 2, 3][i])
      })

      it('component \\* filter test', () => {
        file.contents = Buffer.from(page('component=\\*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [20, 5, 2, 3][i])
      })

      it('component \\* module \\* filter test', () => {
        file.contents = Buffer.from(page('component=\\*,module=\\*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [40, 5, 2, 3][i])
      })

      it('component c2 filter test', () => {
        file.contents = Buffer.from(page('component=c2'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [10, 5, 2, 3][i])
      })

      it('version * filter test', () => {
        file.contents = Buffer.from(page('version=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [30, 5, 2, 3][i])
      })

      it('version master filter test', () => {
        file.contents = Buffer.from(page('version=master'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [10, 5, 2, 3][i])
      })

      it('module * filter test', () => {
        file.contents = Buffer.from(page('module=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [20, 5, 2, 3][i])
      })

      it('module module filter test', () => {
        file.contents = Buffer.from(page('module=module'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [10, 5, 2, 3][i])
      })

      it('relative topic filter test', () => {
        file.contents = Buffer.from(page('relative=topic/**'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [5, 5, 2, 3][i])
      })

      it('relative topic filter test', () => {
        file.contents = Buffer.from(page('relative=topic/\\**'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [5, 5, 2, 3][i])
      })

      it('relative topic filter test 2', () => {
        file.contents = Buffer.from(page('relative=topic/**'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [5, 5, 2, 3][i])
      })

      it('attribute even present filter test', () => {
        file.contents = Buffer.from(page('attributes=even'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [4, 2, 1, 2][i])
      })
      it('attribute even not present filter test', () => {
        file.contents = Buffer.from(page('attributes=!even'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig).convert()
        checkDoc(doc, [6, 3, 1, 3][i])
      })
    })
  })
})
