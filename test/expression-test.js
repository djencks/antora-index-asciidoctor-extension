'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension expression tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  it('ulist test', () => {
    file.contents = Buffer.from(`= Ulist

indexList::[\`The Link $\{$xref} to *$\{mod2}*\`]`)
    const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
    expect(doc.convert()).to.equal(`<div class="ulist">
<ul>
<li>
<p>The Link <a href="https://example.com/c/1.0/page1.html" class="xref page">1.0@c1:ROOT:page1.adoc</a> to <strong>1</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/page2.html" class="xref page">1.0@c1:ROOT:page2.adoc</a> to <strong>0</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/page3.html" class="xref page">1.0@c1:ROOT:page3.adoc</a> to <strong>1</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/page4.html" class="xref page">1.0@c1:ROOT:page4.adoc</a> to <strong>0</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/page5.html" class="xref page">1.0@c1:ROOT:page5.adoc</a> to <strong>1</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/topic/page1.html" class="xref page">1.0@c1:ROOT:topic/page1.adoc</a> to <strong>1</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/topic/page2.html" class="xref page">1.0@c1:ROOT:topic/page2.adoc</a> to <strong>0</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/topic/page3.html" class="xref page">1.0@c1:ROOT:topic/page3.adoc</a> to <strong>1</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/topic/page4.html" class="xref page">1.0@c1:ROOT:topic/page4.adoc</a> to <strong>0</strong></p>
</li>
<li>
<p>The Link <a href="https://example.com/c/1.0/topic/page5.html" class="xref page">1.0@c1:ROOT:topic/page5.adoc</a> to <strong>1</strong></p>
</li>
</ul>
</div>`)
  })

  it('dlist test', () => {
    file.contents = Buffer.from(`= Dlist

indexDescriptionList::[\`The Link $\{$xref} to *$\{mod2}*\`,\`$\{description} and $\{mod3}\`]
`)
    const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
    expect(doc.convert()).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/page1.html" class="xref page">1.0@c1:ROOT:page1.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 1 and 1</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/page2.html" class="xref page">1.0@c1:ROOT:page2.adoc</a> to <strong>0</strong></dt>
<dd>
<p>description 2 and 2</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/page3.html" class="xref page">1.0@c1:ROOT:page3.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 3 and 0</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/page4.html" class="xref page">1.0@c1:ROOT:page4.adoc</a> to <strong>0</strong></dt>
<dd>
<p>description 4 and 1</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/page5.html" class="xref page">1.0@c1:ROOT:page5.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 5 and 2</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/topic/page1.html" class="xref page">1.0@c1:ROOT:topic/page1.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 1 and 1</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/topic/page2.html" class="xref page">1.0@c1:ROOT:topic/page2.adoc</a> to <strong>0</strong></dt>
<dd>
<p>description 2 and 2</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/topic/page3.html" class="xref page">1.0@c1:ROOT:topic/page3.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 3 and 0</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/topic/page4.html" class="xref page">1.0@c1:ROOT:topic/page4.adoc</a> to <strong>0</strong></dt>
<dd>
<p>description 4 and 1</p>
</dd>
<dt class="hdlist1">The Link <a href="https://example.com/c/1.0/topic/page5.html" class="xref page">1.0@c1:ROOT:topic/page5.adoc</a> to <strong>1</strong></dt>
<dd>
<p>description 5 and 2</p>
</dd>
</dl>
</div>`)
  })

  it('table test', () => {
    file.contents = Buffer.from(`= Table

[cols=3*,options="header"]
|===
| attribute
| link
| trimmed attribute
|===

indexTable::[cellformats="description.slice(12)|\`Link $\{$xref} ($\{mod2})\`|\`$\{$xref} and $\{mod3}\`"]
`)
    const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
    expect(doc.convert()).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 33.3333%;">
<col style="width: 33.3334%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">attribute</th>
<th class="tableblock halign-left valign-top">link</th>
<th class="tableblock halign-left valign-top">trimmed attribute</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">1</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/page1.html" class="xref page">1.0@c1:ROOT:page1.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/page1.html" class="xref page">1.0@c1:ROOT:page1.adoc</a> and 1</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">2</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/page2.html" class="xref page">1.0@c1:ROOT:page2.adoc</a> (0)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/page2.html" class="xref page">1.0@c1:ROOT:page2.adoc</a> and 2</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">3</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/page3.html" class="xref page">1.0@c1:ROOT:page3.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/page3.html" class="xref page">1.0@c1:ROOT:page3.adoc</a> and 0</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">4</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/page4.html" class="xref page">1.0@c1:ROOT:page4.adoc</a> (0)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/page4.html" class="xref page">1.0@c1:ROOT:page4.adoc</a> and 1</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">5</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/page5.html" class="xref page">1.0@c1:ROOT:page5.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/page5.html" class="xref page">1.0@c1:ROOT:page5.adoc</a> and 2</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">1</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/topic/page1.html" class="xref page">1.0@c1:ROOT:topic/page1.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/topic/page1.html" class="xref page">1.0@c1:ROOT:topic/page1.adoc</a> and 1</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">2</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/topic/page2.html" class="xref page">1.0@c1:ROOT:topic/page2.adoc</a> (0)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/topic/page2.html" class="xref page">1.0@c1:ROOT:topic/page2.adoc</a> and 2</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">3</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/topic/page3.html" class="xref page">1.0@c1:ROOT:topic/page3.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/topic/page3.html" class="xref page">1.0@c1:ROOT:topic/page3.adoc</a> and 0</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">4</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/topic/page4.html" class="xref page">1.0@c1:ROOT:topic/page4.adoc</a> (0)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/topic/page4.html" class="xref page">1.0@c1:ROOT:topic/page4.adoc</a> and 1</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">5</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Link <a href="https://example.com/c/1.0/topic/page5.html" class="xref page">1.0@c1:ROOT:topic/page5.adoc</a> (1)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><a href="https://example.com/c/1.0/topic/page5.html" class="xref page">1.0@c1:ROOT:topic/page5.adoc</a> and 2</p></td>
</tr>
</tbody>
</table>`)
  })
})
