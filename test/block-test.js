'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension block tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkDoc (doc, count, match) {
    expect(doc).to.not.equal(null)
    expect(doc.blocks.length).to.eq(count)
    doc.blocks.forEach((block, i) => {
      expect(block.context).to.eq('document')
    })
  }

  ;[
    {
      page: (filter) => `

[indexBlock,'xref=$xref,description,p=description.slice(12)']
----
== Link {xref}

{description}

page: {p}
----
`,
    },
  ].forEach(({ page, matches }) => {
    describe(`match: ${matches}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, matches)
        expect(doc.convert()).to.equal(`<div class="sect1">
<h2 id="_link_1_0c1rootpage1_adoc">Link <a href="https://example.com/c/1.0/page1.html" class="xref page">1.0@c1:ROOT:page1.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 1</p>
</div>
<div class="paragraph">
<p>page: 1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1rootpage2_adoc">Link <a href="https://example.com/c/1.0/page2.html" class="xref page">1.0@c1:ROOT:page2.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 2</p>
</div>
<div class="paragraph">
<p>page: 2</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1rootpage3_adoc">Link <a href="https://example.com/c/1.0/page3.html" class="xref page">1.0@c1:ROOT:page3.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 3</p>
</div>
<div class="paragraph">
<p>page: 3</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1rootpage4_adoc">Link <a href="https://example.com/c/1.0/page4.html" class="xref page">1.0@c1:ROOT:page4.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 4</p>
</div>
<div class="paragraph">
<p>page: 4</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1rootpage5_adoc">Link <a href="https://example.com/c/1.0/page5.html" class="xref page">1.0@c1:ROOT:page5.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 5</p>
</div>
<div class="paragraph">
<p>page: 5</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1roottopicpage1_adoc">Link <a href="https://example.com/c/1.0/topic/page1.html" class="xref page">1.0@c1:ROOT:topic/page1.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 1</p>
</div>
<div class="paragraph">
<p>page: 1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1roottopicpage2_adoc">Link <a href="https://example.com/c/1.0/topic/page2.html" class="xref page">1.0@c1:ROOT:topic/page2.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 2</p>
</div>
<div class="paragraph">
<p>page: 2</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1roottopicpage3_adoc">Link <a href="https://example.com/c/1.0/topic/page3.html" class="xref page">1.0@c1:ROOT:topic/page3.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 3</p>
</div>
<div class="paragraph">
<p>page: 3</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1roottopicpage4_adoc">Link <a href="https://example.com/c/1.0/topic/page4.html" class="xref page">1.0@c1:ROOT:topic/page4.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 4</p>
</div>
<div class="paragraph">
<p>page: 4</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_link_1_0c1roottopicpage5_adoc">Link <a href="https://example.com/c/1.0/topic/page5.html" class="xref page">1.0@c1:ROOT:topic/page5.adoc</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>description 5</p>
</div>
<div class="paragraph">
<p>page: 5</p>
</div>
</div>
</div>`)
      })
    })
  })
})
