'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
//Needed to set up globals for report.js
// eslint-disable-next-line no-unused-vars
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

var pipeline

describe('indexpage extension tests', () => {
  let contentCatalog
  let file
  const log = []
  let baseCount

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
    contentCatalog.addFile({
      src: Object.assign({}, file.src, { family: 'example', relative: 'template1.adoc' }),
      // eslint-disable-next-line no-template-curly-in-string
      contents: Buffer.from('= This is the template for ${doctitle}\n${$resourceid}\n${src.component}\n${src.version}\n${src.module}\n${src.family}\n${src.relative}'),
    })
    contentCatalog.addFile({
      src: Object.assign({}, file.src, { family: 'example', relative: 'template2.adoc' }),
      // eslint-disable-next-line no-template-curly-in-string
      contents: Buffer.from('= This is the template for ${doctitle}\n${xbasename}\n${xextension}'),
    })
    contentCatalog.addFile({
      src: Object.assign({}, file.src, { version: 'master', family: 'example', relative: 'template2.adoc' }),
      // eslint-disable-next-line no-template-curly-in-string
      contents: Buffer.from('= This is the template for ${doctitle}\n${xbasename}\n${xextension}'),
    })
    baseCount = contentCatalog.getFiles().length
    pipeline = {

      on: (name, fn) => { pipeline[name] = fn },

      getLogger: (name) => {
        return {
          trace: (args) => log.push(args),
          debug: (args) => log.push(args),
          info: (args) => log.push(args),
          warn: (args) => log.push(args),
          isLevelEnabled: () => true,
        }
      },

    }
  })

  it('basic test', () => {
    indexExtension.register(pipeline, { playbook: {}, config: { trace: true, indexPages: [] } })
    expect(typeof pipeline.contentClassified).to.equal('function')
  })

  it('indexPage test', () => {
    indexExtension.register(pipeline, {
      playbook: {},
      config: {
        log_level: 'debug',
        indexPages: [
          {
            query: {
              version: '1.0',
              component: 'c1',
              module: 'ROOT',
              family: 'page',
              relative: '*.adoc',
            },
            templateId: {
              family: 'example',
              relative: 'template1.adoc',
            },
            target: {
              match: '(?<name>*)',
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${name}`',
            },
          },
        ],
      },
    })
    expect(typeof pipeline.contentClassified).to.equal('function')
    pipeline.contentClassified({ contentCatalog, asciidocConfig: {} })
    expect(contentCatalog.getFiles().length).to.equal(baseCount + 6)
    const pages = contentCatalog.findBy({ family: 'page' })
    const lastPage = pages[pages.length - 1]
    const generatedText = lastPage.contents.toString()
    expect(generatedText).to.equal('= This is the template for index.adoc\n1.0@c1:ROOT:page$index.adoc\nc1\n1.0\nROOT\npage\nindex.adoc')
    expect(lastPage.out.path).to.equal('c1/1.0/generated/index.html')
  })

  it('indexPage test 2', () => {
    indexExtension.register(pipeline, {
      playbook: {},
      config: {
        log_level: 'debug',
        indexPages: [
          {
            query: {
              // version: '1.0',
              component: 'c1',
              module: 'ROOT',
              family: 'page',
              relative: '*.adoc',
            },
            templateId: {
              family: 'example',
              relative: 'template1.adoc',
            },
            target: {
              match: '(?<name>*)',
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${name}`',
            },
          },
        ],
      },
    })
    expect(typeof pipeline.contentClassified).to.equal('function')
    pipeline.contentClassified({ contentCatalog, asciidocConfig: {} })
    expect(contentCatalog.getFiles().length).to.equal(baseCount + 6)
    const pages = contentCatalog.findBy({ family: 'page' })
    const lastPage = pages[pages.length - 1]
    const generatedText = lastPage.contents.toString()
    expect(generatedText).to.equal('= This is the template for index.adoc\n1.0@c1:ROOT:page$index.adoc\nc1\n1.0\nROOT\npage\nindex.adoc')
    expect(lastPage.out.path).to.equal('c1/1.0/generated/index.html')
  })

  it('indexPage extract test', () => {
    indexExtension.register(pipeline, {
      playbook: {},
      config: {
        trace: true,
        indexPages: [
          {
            query: {
              version: '1.0',
              component: 'c1',
              module: 'ROOT',
              family: 'page',
              relative: '*.adoc',
            },
            templateId: {
              family: 'example',
              relative: 'template2.adoc',
            },
            extract: [
              { path: 'src.relative', match: '(?<xbasename>*).(?<xextension>*)' },
            ],
            target: {
              match: '(?<name>*)',
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${name}`',
            },
          },
        ],
      },
    })
    expect(typeof pipeline.contentClassified).to.equal('function')
    pipeline.contentClassified({ contentCatalog, asciidocConfig: {} })
    expect(contentCatalog.getFiles().length).to.equal(baseCount + 6)
    const pages = contentCatalog.findBy({ family: 'page' })
    const lastPage = pages[pages.length - 1]
    const generatedText = lastPage.contents.toString()
    expect(generatedText).to.equal('= This is the template for index.adoc\nindex\nadoc')
    expect(lastPage.out.path).to.equal('c1/1.0/generated/index.html')
  })

  it('indexPage extract test 2', () => {
    indexExtension.register(pipeline, {
      playbook: {},
      config: {
        trace: true,
        indexPages: [
          {
            query: {
              // version: '1.0',
              component: 'c1',
              module: 'ROOT',
              family: 'page',
              relative: '*.adoc',
            },
            templateId: {
              family: 'example',
              relative: 'template2.adoc',
            },
            extract: [
              { path: 'src.relative', match: '(?<xbasename>*).(?<xextension>*)' },
            ],
            target: {
              match: '(?<name>*)',
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${name}`',
            },
          },
        ],
      },
    })
    expect(typeof pipeline.contentClassified).to.equal('function')
    pipeline.contentClassified({ contentCatalog, asciidocConfig: {} })
    expect(contentCatalog.getFiles().length).to.equal(baseCount + 10 + 1)
    const pages = contentCatalog.findBy({ family: 'page' })
    const lastPage = pages[pages.length - 1]
    const generatedText = lastPage.contents.toString()
    expect(generatedText).to.equal('= This is the template for c1 master ROOT  5\npage5\nadoc')
    expect(lastPage.out.path).to.equal('c1/generated/page5.html')
  })
})
